<?php
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);
    
    session_start();
    
    if (!isset($_SESSION["lang"])) {
      $_SESSION["lang"] = "es";
    }
    if (isset($_GET["lang"])) {
      $_SESSION["lang"] = $_GET["lang"];
    }
    
    require "lang-" . $_SESSION["lang"] . ".php";
    
    // Libraries
    include("lib/const.php");
    include("lib/template.php");
    
    // Header
    $header = new Template("templates/header.tpl");
    $header->set("lang", $_SESSION["lang"]);
    $header->set("title", "Sachenka Vieternelle");
    $header->set("canonical", URL);
    if (array_key_exists(4, $txt)) {
      $header->set("nav-menu", $txt[4]);
    } else {
      $header->set("nav-menu", "");
    }
    echo $header->output();
    
    // Main content
    $main = new Template("templates/legal.tpl");
    
    if (array_key_exists(26, $txt)) {
      $main->set("legal", $txt[26]);
    } else {
      $main->set("legal", "");
    }
    
    echo $main->output();
    
    // Prepare the copyright year
    $copyright_year = date("Y");
    
    if ($copyright_year != "2022") {
        $copyright_year = "2022-" . $copyright_year;
    }
    
    // Footer
    $footer = new Template("templates/footer.tpl");
    if (array_key_exists(1, $txt)) {
      $footer->set("notice", $txt[1]);
    } else {
      $footer->set("notice", "");
    }
    if (array_key_exists(2, $txt)) {
      $footer->set("icons", $txt[2]);
    } else {
      $footer->set("icons", "");
    }
    $footer->set("year", $copyright_year);
    $footer->set("extra-scripts", "");
    echo $footer->output();
?>