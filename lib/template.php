<?php
    // A Template class
    class Template {
        protected $file;
        protected $values = array();
        
        // A constructor
        public function __construct($file) {
            $this->file = $file;
        }
        
        // Set the values
        public function set($key, $value) {
            $this->values[$key] = $value;
        }
        
        // Output the content
        public function output() {
            // Check if the template file exists
            if (!file_exists($this->file)) {
                return "Error loading template file ($this->file).";
            }
            
            // Load the template file
            $output = file_get_contents($this->file);
            
            // Assign the values
            foreach ($this->values as $key => $value) {
                $tagToReplace = "[@$key]";
                if (!is_null($value)) {
                  $output = str_replace($tagToReplace, $value, $output);
                } else {
                  $output = "";
                }
            }
            
            return $output;
        }
    }
?>
