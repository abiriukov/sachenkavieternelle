<?php
$txt = [
  "Bienvenue sur mon site web",
  "Mentions légales et politique de confidentialité",
  "Icônes par",
  "Si tu veux me contacter pour l’une ou l’autre de ces activités",
  "Menu de navigation",
  "Je m'appelle Sachenka Vieternelle, je suis née en France. Un de mes hobbies est de dessiner des âmes, j'aime beaucoup les gens, leurs animaux de compagnie et l'énergie qu'ils dégagent.</p><p>J’ai toujours été très sensible, à travers de mon parcours de vie, J’ai appris à comprendre le fonctionnement de ma sensibilité, mieux la voir et comment l'exprimer.</p><p>Mon cheminement intérieur m'a amenée a développer les dessins d'âmes comme outil pour aider les autres à se rappeler qui ils sont vraiment et révéler la beauté de leur être profond. Je crée un dessin sur des feuilles de différentes tailles, peint avec des crayons de couleur, à partir d'une photo ou d'un entretien personnel.",
  "J'assiste habituellement à des foires pour vendre mes dessins d'âmes et mes collections de cartes à messages faites par moi. Je suis passionnée par les fleurs et j'adore les photographier. J'aide également les gens à mettre de l'ordre dans leurs maisons et leurs environnements,  suivant la " . '"' . "Méthode Marie Kondo" . '"',
  '"' . "La première fois que je vis les dessins d’âmes d’Sachenka j’ai senti en moi la beauté et la pureté de l’être divin.",
  "En chacune de ses créations Sachenka révèle avec amour l’authenticité qui émane de ton âme, le mandala de ton soi supérieur. Tout ce que tu embrasses du plus profond de ton cœur, et quand tu le contemples c’est comme si tu touchais ta divinité intérieure.",
  "Chaque " . '""' . "Dessin de l'âme" . '"' . " est unique et irremplaçable, tout comme le sont les âmes. Tous différents dans leur façon originale d'exprimer la création, dans leur façon d'aimer avec la beauté de leur musique, leur danse, leur parfum et leur couleur. Un dessin protecteur pour vous, pour votre maison et pour les autres.",
  "Son travail est en même temps une protection pour toi et ta maison, ainsi que pour tous ceux qui t’entourent.",
  "Merci Sachenka d'avoir donné vie à l'essence la plus pure de la Divinité des âmes." . '"',
  "MariLoli",
  "Je m’occupe aussi d’amener la Magie de l’ordre dans ta vie. A commencer par ta propre maison. Je te soutiens pour organiser tes espaces privés, en mettant l’équilibre et l’ordre autour de toi. Recycler et/ou jeter ce qui n’est plus nécessaire...",
  "<a href='https://fr.wikipedia.org/wiki/Marie_Kondo' target='_blank 'rel='noreferrer'>LA MAGIE DU RANGEMENT PAR MARIE KONDO</a>",
  "J’assiste régulièrement à des foires locales pour faire connaitre et vendre mes créations; Collection de jeux de cartes et ses paroles de sagesse, dessins d’âme, magie de l’ordre…",
  "Témoignage",
  "<p>C’est comme comprendre ta nature, ton monde intérieur, ton essence.</p>
  <p>Même si on ne peut y mettre des mots, tu reconnais le grand potentiel d’harmonie qui t’habite.</p>
  <p>Tu sens que ton être est simplement Amour, Paix, Créativité…Je sens un grand calme à contempler toute cette beauté que ma peur ne me laissait pas voir.</p>
  <p>On peut y voir refléter la joie et l’innocence qui m’amènent entrevoir la plénitude latente ici et maintenant.</p>
  <p>Un regard direct à la pleine lumière que nous sommes.</p>
  <h3>Aintzane</h3>",
  "<p>Me mettre face à mon dessin quelques minutes c’est entrer dans un espace de paix, de tranquillité. C’est un moment pour m’enfuir du traintrain quotidien et m’autoriser à regarder en mon intérieur. Dans le dessin je vois les 7 chakras de façon claire. </p> 
  <p>Je mets ma conscience en eux et je peux facilement remettre de l’ordre pour continuer la journée.</p> 
  <p>L’interprétation de Sachenka une fois mon dessin d’âme terminé m’a semblé être des plus justes et aiguisées: c’est vrai que je me considère comme quelqu’un capable d’écouter et d´aider les autres de façon désintéressée et de les amener à transformer leurs émotions. Merci Sachenka pour ton délicat travail.</p> 
  <p>Merci Sachenka pour ton délicat travail.</p>
  <h3>María Antonia Sánchez Murillo</h3>",
  "<p>Etonnement, Sachenka a réussi à se connecter à mon chat, Minouche, un chat philosophe avec le regard pénétrant.</p>
  <p>Elle a capté intuitivement la vibration qui émane de lui. Il irradie et transmet à son entourage sa chaleureuse énergie, qui te calme.</p>
  <p>Après Sachenka a examiné mon âme. Que se passe-t’il en moi? Dans mon être profond?</p>
  <p>J’ai découvert dans son dessin, surprise, qu’elle a senti de façon toute a fait juste que je suis ici sans y être. Que je vis dans les airs mais malgré tout ancrée dans mes relations avec les autres.</p>
  <p>Son dessin m’amène à réfléchir sur moi-même, m’ouvre des portes pour créer une meilleure harmonie, une meilleure vibration et me réconforte sur ce que je connais de moi.</p>
  <p>Elle m’a aidé à comprendre pourquoi il m’est si difficile de m’ancrer sur cette terre.</p>
  <h3>Sacha</h3>",
  "Mes dessins d'âmes se presentent en trois tailles, n'hésites pas à me contacter si tu souhaites autre chose.",
  "Témoignages",
  "Dessins d’âmes",
  "La magie du rangement",
  "Jeu: Fleurs et paroles de sagesse",
  "<p>Le jeu se compose de 42 cartes avec des photos de fleurs avec des mots de sagesse.</p>
  <p>7 familles et 7 couleurs.</p> 
  <p>Le prix du jeu complet est de 12 €. *</p>
  <p>* Le jeu de cartes est en espagnol.</p>",
  "<h2 class='text-center'>Politique de confidentialité et mentions légales</h2>
            <h3>Politique de confidentialité</h3>
            <p>Grâce à ce site Web, aucune donnée personnelle n'est collectée auprès des utilisateurs à leur insu, ni transférée à des tiers.</p>
            <p>Afin de vous offrir le meilleur service et afin de faciliter l'utilisation, le nombre de pages visitées, le nombre de visites, ainsi que l'activité des visiteurs et leur fréquence d'utilisation sont analysés. À ces fins, le site Web de Sachenka Vieternelle utilise les informations statistiques préparées par le fournisseur d'accès Internet.</p>
            <p>Le site Web de Sachenka Vieternelle n'utilise pas de cookies pour collecter des informations auprès des utilisateurs, ni n'enregistre les adresses IP d'accès.</p>
            <p>Le site Web de Sachenka Vieternelle contient des liens vers des sites Web tiers, dont les politiques de confidentialité sont étrangères à celles de l'AEPD. En accédant à ces sites Web, vous pouvez décider d'accepter ou non leurs politiques de confidentialité et de cookies. En général, si vous naviguez sur Internet, vous pouvez accepter ou refuser les cookies tiers à partir des options de configuration de votre navigateur.</p>
            <h3>Mentions légales</h3>
            <p>Conformément à l'article 10 de la loi 34/2002, du 11 juillet, services de la société de l'information et du commerce électronique (LSSICE) ci-dessous sont les données d'identification de la société:</p>
            <p>Raison sociale : Sachenka Viret (personne physique)</p>
            <p>NIF: 80107100248</p>
            <p>Adresse: Ayerbe (Huesca)</p>
            <p>Téléphone: +33 637085 233</p>
            <p>Messagerie électronique: info@sachenkavieternelle.com</p>
            <h4>OBJET DU SITE WEB</h4>
            <p>Le but de ce site Web est seulement informatif. Les présentes mentions légales régissent l'utilisation du site : www.sachenkavieternelle.com</p>
            <h4>LÉGISLATION</h4>
            <p>En général, les relations entre Sachenka Viret et les utilisateurs de son site Web sont soumises à la législation et à la juridiction espagnoles.</p>
            <h4>UTILISATION ET ACCÈS PAR L'UTILISATEUR</h4>
            <p>L'Utilisateur est informé, et accepte, que l'accès à ce site n'implique en aucun cas le début d'une relation commerciale avec Sachenka Viret.</p>
            <h4>PROPRIÉTÉ INTELLECTUELLE ET INDUSTRIELLE</h4>
            <p>Les droits de propriété intellectuelle sur le contenu des pages Web, leur conception graphique et leurs codes sont la propriété d'Sachenka Viret et, par conséquent, leur reproduction, leur distribution, leur communication publique, leur transformation ou toute autre activité pouvant être effectuée avec le contenu de son site Web sont interdites, même en citant les sources, sauf accord écrit d'Sachenka Viret.</p>
            <h4>CONTENU WEB ET LIENS</h4>
            <p>Sachenka Viret n'assume aucune responsabilité pour les informations contenues dans les sites Web de tiers auxquels il est possible d'accéder via des « liens » ou des liens du site Web www.sachenkavieternelle.com. La présence de « liens » ou de liens sur le site Internet d'Sachenka Viret est fournie à titre informatif uniquement et n'implique en aucun cas une suggestion, une invitation ou une recommandation à leur sujet.</p>"
]
?>