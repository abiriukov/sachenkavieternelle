<?php
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);
    
    session_start();
    
    if (!isset($_SESSION["lang"])) {
      $_SESSION["lang"] = "es";
    }
    if (isset($_GET["lang"])) {
      $_SESSION["lang"] = $_GET["lang"];
    }
    
    require "lang-" . $_SESSION["lang"] . ".php";
    
    // Libraries
    include("lib/const.php");
    include("lib/template.php");
    
    // Header
    $header = new Template("templates/header.tpl");
    $header->set("lang", $_SESSION["lang"]);
    
    if (array_key_exists(22, $txt)) {
      $header->set("title", "Sachenka Vieternelle - " . $txt[22]);
      $header->set("soul_drawings", $txt[22]);
    } else {
      $header->set("title", "Sachenka Vieternelle");
      $header->set("soul_drawings", "");
    }
    
    $header->set("canonical", URL);
    if (array_key_exists(4, $txt)) {
      $header->set("nav-menu", $txt[4]);
    } else {
      $header->set("nav-menu", "");
    }
    echo $header->output();
    
    // Get current URI
    $uri = $_SERVER['REQUEST_URI'];
    $uri = substr($uri, strrpos($uri, '/'));
    
    // Main content
    if ($uri == "/" || substr($uri, 0, 2) == "/?") {
      $main = new Template("templates/main.tpl");
    } else {
      $main = new Template("templates" . $uri . ".tpl");
    }
    
    if (array_key_exists(3, $txt)) {
      $main->set("contact", $txt[3]);
    } else {
      $main->set("contact", "");
    }
    
    if (array_key_exists(5, $txt)) {
      $main->set("intro1", $txt[5]);
    } else {
      $main->set("intro1", "");
    }
    
    if (array_key_exists(6, $txt)) {   
      $main->set("intro2", $txt[6]);
    } else {
      $main->set("intro2", "");
    }
    
    if (array_key_exists(7, $txt)) {
      $main->set("intro3", $txt[7]);
    } else {
      $main->set("intro3", "");
    }
    
    if (array_key_exists(8, $txt)) {
      $main->set("intro4", $txt[8]);
    } else {
      $main->set("intro4", "");
    }
    
    if (array_key_exists(9, $txt)) {
      $main->set("intro5", $txt[9]);
    } else {
      $main->set("intro5", "");
    }
    
    if (array_key_exists(10, $txt)) {
      $main->set("intro6", $txt[10]);
    } else {
      $main->set("intro6", "");
    }
    
    if (array_key_exists(11, $txt)) {
      $main->set("intro7", $txt[11]);
    } else {
      $main->set("intro7", "");
    }
    
    if (array_key_exists(12, $txt)) {
      $main->set("intro8", $txt[12]);
    } else {
      $main->set("intro8", "");
    }
    
    if (array_key_exists(16, $txt)) {
      $main->set("testimony", $txt[16]);
    } else {
      $main->set("testimony", "");
    }
    
    if (array_key_exists(21, $txt)) {
      $main->set("testimonies", $txt[21]);
    } else {
      $main->set("testimonies", "");
    }
    
    if (array_key_exists(17, $txt)) {
      $main->set("testimony1", $txt[17]);
    } else {
      $main->set("testimony1", "");
    }
    
    if (array_key_exists(18, $txt)) {
      $main->set("testimony2", $txt[18]);
    } else {
      $main->set("testimony2", "");
    }
    
    if (array_key_exists(19, $txt)) {
      $main->set("testimony3", $txt[19]);
    } else {
      $main->set("testimony3", "");
    }
    
    if (array_key_exists(22, $txt)) {
      $main->set("soul_drawings", $txt[22]);
    } else {
      $main->set("soul_drawings", "");
    }
    
    $path = "/img/gallery/";
    $abspath = __DIR__ . $path;
    $file_count = count(glob($abspath . "*.{png,jpg,jpeg,webp}", GLOB_BRACE));
    if ($file_count > 0) {
        $file_dir = opendir($abspath);
        $gallery = "";
        $files = array();
        while ($files[] = readdir($file_dir));
        sort($files);
        closedir($file_dir);
        //while ($file = readdir($file_dir)) {
        foreach ($files as $file) {
            $ext = pathinfo($file, PATHINFO_EXTENSION);
            $file_name = pathinfo($file, PATHINFO_FILENAME);
            $img_name = ucwords(str_replace('-', ' ', $file_name));
            $ext_array = ['png', 'jpg', 'jpeg', 'webp'];
            if (in_array($ext, $ext_array)) {
                $file_path = substr($_SERVER['REQUEST_URI'], 0, strrpos($_SERVER['REQUEST_URI'], "/")) . $path . $file;
                $thumbnail_path = substr($_SERVER['REQUEST_URI'], 0, strrpos($_SERVER['REQUEST_URI'], "/")) . $path . 'thumbnails/' . $file;
                $gallery .= '<div class="col-md-6 col-lg-4 text-center py-3">
                            <a href="' . $file_path . '" title="' . $img_name . '">
                            <img class="img-fluid rounded-xl shadow-custom" src="' . $thumbnail_path . '" alt="Imagen ' . $img_name . '" loading="lazy">' .
                            /*<h4>' . $img_name . '</h4>*/
                            '</a>
                            </div>';         
            }
        }
        $main->set("gallery", $gallery);
    }
    
    if (array_key_exists(23, $txt)) {
      $main->set("magic", $txt[23]);
    } else {
      $main->set("magic", "");
    }
    
    if (array_key_exists(13, $txt)) {
      $main->set("order1", $txt[13]);
    } else {
      $main->set("order1", "");
    }
    
    if (array_key_exists(14, $txt)) {
      $main->set("order2", $txt[14]);
    } else {
      $main->set("order2", "");
    }
    
    if (array_key_exists(15, $txt)) {
      $main->set("order3", $txt[15]);
    } else {
      $main->set("order3", "");
    }
    
    if (array_key_exists(20, $txt)) {
      $main->set("order4", $txt[20]);
    } else {
      $main->set("order4", "");
    }
    
    $path = "/img/gallery-order/";
    $abspath = __DIR__ . $path;
    $file_count = count(glob($abspath . "*.{png,jpg,jpeg,webp}", GLOB_BRACE));
    if ($file_count > 0) {
        $file_dir = opendir($abspath);
        $gallery = "";
        $files = array();
        while ($files[] = readdir($file_dir));
        sort($files);
        closedir($file_dir);
        //while ($file = readdir($file_dir)) {
        foreach ($files as $file) {
            $ext = pathinfo($file, PATHINFO_EXTENSION);
            $file_name = pathinfo($file, PATHINFO_FILENAME);
            $img_name = ucwords(str_replace('-', ' ', $file_name));
            $ext_array = ['png', 'jpg', 'jpeg', 'webp'];
            if (in_array($ext, $ext_array)) {
                $file_path = substr($_SERVER['REQUEST_URI'], 0, strrpos($_SERVER['REQUEST_URI'], "/")) . $path . $file;
                $gallery .= '<div class="col-md-6 col-lg-4 text-center py-3">
                            <a href="' . $file_path . '" title="' . $img_name . '">
                            <img class="img-fluid rounded-xl shadow-custom" src="' . $file_path . '" alt="Imagen ' . $img_name . '" loading="lazy">' .
                            /*<h4>' . $img_name . '</h4>*/
                            '</a>
                            </div>';         
            }
        }
        $main->set("gallery-order", $gallery);
    } 
    
    if (array_key_exists(24, $txt)) {
      $main->set("cards", $txt[24]);
    } else {
      $main->set("cards", "");
    }
    
    if (array_key_exists(25, $txt)) {
      $main->set("cards-text", $txt[25]);
    } else {
      $main->set("cards-text", "");
    }     
    
    echo $main->output();
    
    // Prepare the copyright year
    $copyright_year = date("Y");
    
    if ($copyright_year != "2022") {
        $copyright_year = "2022-" . $copyright_year;
    }
    
    // Footer
    $footer = new Template("templates/footer.tpl");
    if (array_key_exists(1, $txt)) {
      $footer->set("notice", $txt[1]);
    } else {
      $footer->set("notice", "");
    }
    if (array_key_exists(2, $txt)) {
      $footer->set("icons", $txt[2]);
    } else {
      $footer->set("icons", "");
    }
    $footer->set("year", $copyright_year);
    $footer->set("extra-scripts", "");
    echo $footer->output();
?>