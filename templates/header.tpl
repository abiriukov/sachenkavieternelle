<!DOCTYPE html>
<html lang=[@lang]>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="Wildions">
    <title>[@title]</title>
    <link rel="canonical" href="[@canonical]">
    <link rel="icon" href="/favicon.ico" type="image/x-icon">
    <link rel="icon" href="/favicon.png" type="image/png">
    <link rel="apple-touch-icon" sizes="120x120" href="apple-touch-icon-120x120-precomposed.png">
    <link rel="apple-touch-icon" sizes="152x152" href="apple-touch-icon-152x152-precomposed.png">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
    <link href="css/normalize.css" rel="stylesheet" type='text/css'>    
    <link href="css/main.css" rel="stylesheet" type='text/css'>
    <link rel="stylesheet" href="css/blueimp-gallery.min.css" />
  </head>
  <body>
    <header>
      <nav id="main-nav" class="navbar navbar-expand-lg navbar-light bg-bars">
        <div class="container-fluid">
          <!-- <a class="navbar-brand" href="#">
            <img src="img/logo.png" class="d-inline-block align-middle" alt="Sachenka Vieternelle" loading="lazy"> 
            <span class="d-none d-lg-inline align-middle fs-1 me-auto">Sachenka Vieternelle</span>
            <span class="d-lg-none align-middle fs-1 text-wrap me-auto">Sachenka Vieternelle</span>
          </a> -->
          <div class="row justify-content-start navbar-brand">
            <div class="col-3 col-lg-3">
              <a href="https://sachenkavieternelle.com">
                <img src="img/logo.png" class="align-middle" alt="Sachenka Vieternelle" loading="lazy">
              </a>
            </div>
            <div class="col-4">
              <h1 class="mt-0 mb-2 mt-lg-2 mb-lg-0 lh-1">
                <span class="d-none d-lg-inline align-middle fs-1 text-nowrap">Sachenka Vieternelle</span>
                <span class="d-lg-none align-middle fs-1 text-wrap">Sachenka Vieternelle</span>
              </h1>
              <h2 class="lh-1">
                <span class="d-none d-lg-inline align-middle fs-2 text-nowrap">[@soul_drawings]</span>
                <span class="d-lg-none align-middle fs-2 text-nowrap">[@soul_drawings]</span>
              </h2>
            </div>
          </div>
          <div class="navbar-nav flex-row me-auto me-lg-3 ms-lg-auto">
            <a class="nav-link" href="tel:+33637085233">
              <img class="lang-icon" src="img/icons/icons8-phone-96.png" alt="Tel.">
            </a>
            <a class="nav-link" href="https://wa.me/33637085233">
              <img class="lang-icon" src="img/icons/icons8-whatsapp-96.png" alt="Whatsapp">
            </a>
          </div>
          <div class="navbar-nav flex-row ms-auto ms-lg-0">
            <a class="nav-link" href="?lang=es">
              <img class="lang-icon" src="img/icons/icons8-spain-96.png" alt="ES">
            </a>
            <a class="nav-link" href="?lang=en">
              <img class="lang-icon" src="img/icons/icons8-great-britain-96.png" alt="EN">
            </a>
            <a class="nav-link" href="?lang=fr">
              <img class="lang-icon" src="img/icons/icons8-france-96.png" alt="FR">
            </a>
          </div>
        </div>
      </nav>
    </header>