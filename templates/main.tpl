    <main id="main">
      <div class="containter px-4">
        <div class="row flex-lg-row-reverse align-items-center g-5 py-5">
          <div class="col col-sm-8 col-lg-6">
            <img src="img/av-photo.jpg" class="img-fluid d-block mx-lg-auto rounded-m shadow-m" alt="Alexandra Vieternelle" loading="lazy">
          </div>
          <div class="col-lg-6">
            <p>[@intro1]</p>
            <p>[@intro2]</p>
            <br />
            <p class="testimony fst-italic">[@intro3] [@intro4]</p>
            <!-- <p class="fst-italic">[@intro4]</p> -->
            <p class="testimony fst-italic">[@intro5] [@intro6]</p>
            <!-- <p class="fst-italic">[@intro6]</p> -->
            <p class="testimony fst-italic">[@intro7]</p>
            <p class="testimony fst-italic fw-bold">[@intro8]</p>
          </div>
        </div>
      </div>
      <div id="testimonies-carousel" class="carousel slide bg-bars py-3" data-bs-ride="true">
        <div class="carousel-indicators">
          <button type="button" data-bs-target="#testimonies-carousel" data-bs-slide-to="0" class="active" aria-current="true" aria-label="[@testimony] 1"></button>
          <button type="button" data-bs-target="#testimonies-carousel" data-bs-slide-to="1" aria-label="[@testimony] 2"></button>
          <button type="button" data-bs-target="#testimonies-carousel" data-bs-slide-to="2" aria-label="[@testimony] 3"></button>
        </div>
        <div class="carousel-inner pb-5">
          <h2 class="text-center py-3">[@testimonies]</h2>
          <div class="carousel-item active">
            <div class="container">
              <div class="row align-items-center px-5">
                <div class="col-12 col-md-8 pb-3 pb-md-0">
                  [@testimony1]
                </div>
                <div class="col-12 col-md-4">
                  <img class="img-fluid img-testimony" src="img/testimonies/testimony-1.jpg" alt="Foto [@testimony] 1">
                </div>
              </div>
            </div>
          </div>
          <div class="carousel-item">
            <div class="container">
              <div class="row align-items-center flex-md-row-reverse px-5">
                <div class="col-12 col-md-8 pb-3 pb-md-0">
                  [@testimony2]
                </div>
                <div class="col-12 col-md-4">
                  <img class="img-fluid img-testimony" src="img/testimonies/testimony-2.jpg" alt="Foto [@testimony] 2">
                </div>
              </div>
            </div>
          </div>
          <div class="carousel-item">
            <div class="container">
              <div class="row align-items-center px-5">
                <div class="col-12 col-md-7 pb-3 pb-md-0">
                  [@testimony3]
                </div>
                <div class="col-12 col-md-5">
                  <img class="img-fluid img-testimony" src="img/testimonies/testimony-3.jpg" alt="Foto [@testimony] 3">
                </div>
              </div>
            </div>
          </div>
        </div>
        <button class="carousel-control-prev" type="button" data-bs-target="#testimonies-carousel" data-bs-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="visually-hidden">[@prev]</span>
        </button>
        <button class="carousel-control-next" type="button" data-bs-target="#testimonies-carousel" data-bs-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="visually-hidden">[@next]</span>
        </button>
      </div>
      <div class="container-fluid px-5 my-5">
        <h2 class="text-center py-3">[@soul_drawings]</h2>
        <div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls" aria-label="image gallery" aria-modal="true" role="dialog">
          <div class="slides" aria-live="polite"></div>
          <h3 class="title">[@gallery-title]</h3>
          <a class="prev" aria-controls="blueimp-gallery" aria-label="previous slide" aria-keyshortcuts="ArrowLeft"></a>
          <a class="next" aria-controls="blueimp-gallery" aria-label="next slide" aria-keyshortcuts="ArrowRight"></a>
          <a class="close" aria-controls="blueimp-gallery" aria-label="close" aria-keyshortcuts="Escape"></a>
          <a class="play-pause" aria-controls="blueimp-gallery" aria-label="play slideshow" aria-keyshortcuts="Space" aria-pressed="false" role="button"></a>
          <ol class="indicator"></ol>
        </div>
        <div id="links" class="row align-items-center">
          [@gallery]
        </div>
      </div>
      <div class="container-fluid"></div>
        <div class="row align-items-center bg-bars px-5 py-3">
          <h2 class="text-center py-3">[@magic]</h2>
          <div class="col col-lg-7">
            <div id="blueimp-gallery-order" class="blueimp-gallery blueimp-gallery-carousel" aria-label="image carousel">
              <div class="slides" aria-live="off"></div>
              <h3 class="title">[@gallery-title]</h3>
              <a class="prev" aria-controls="blueimp-gallery-carousel" aria-label="previous slide"></a>
              <a class="next" aria-controls="blueimp-gallery-carousel" aria-label="next slide"></a>
              <a class="close" aria-controls="blueimp-gallery-carousel" aria-label="close"></a>
              <a class="play-pause" aria-controls="blueimp-gallery-carousel" aria-label="play slideshow" aria-pressed="true" role="button"></a>
              <ol class="indicator"></ol>
            </div>
            <div id="links-order" class="row align-items-center d-none">
              [@gallery-order]
            </div>
          </div>
          <div class="col-lg-5">
            <p>[@order1]</p>
            <p>[@order2]</p>
          </div>
        </div>
      </div>
      <div class="container-fluid">
        <h2 class="text-center py-3">[@cards]</h2>
        <div class="row align-items-center px-5 py-3">
          <div class="col-lg-6">
            [@cards-text]
          </div>
          <div class="col-lg-3">
            <img class="img-fluid d-block mx-lg-auto rounded-m shadow-m" src="img/gallery-cards/1.jpg" alt="[@cards] 1">
          </div>
          <div class="col-lg-3">
            <img class="img-fluid d-block mx-lg-auto rounded-m shadow-m" src="img/gallery-cards/2.jpg" alt="[@cards] 2">
          </div>
        </div>
      </div>
      <div class="container-fluid">
        <div class="row align-items-center bg-bars px-5 py-3">
          <div class="col py-3">
            <p>[@order4]</p>
            <table class="table">
              <thead>
                <tr>
                  <th scope="col">A5</th>
                  <th scope="col">A4</th>
                  <th scope="col">A3</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>148x210 mm</td>
                  <td>297x210 mm</td>
                  <td>297x420 mm</td>
                </tr>
                <tr>
                  <td>25 €</td>
                  <td>50 €</td>
                  <td>90 €</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <div class="container py-3">
        <div class="text-center">
          <h3>[@contact]</h3>
        </div>
        <div class="text-center my-3">
          <a href="mailto:ino.conscience@gmail.com"><img src="img/icons/icons8-mail-96.png" alt="Email"></a>
          <a href="https://www.facebook.com/profile.php?id=100086251456888"><img src="img/icons/icons8-facebook-96.png" alt="Facebook"></a>
          <a href="https://instagram.com/sachenkavieternelle?igshid=YmMyMTA2M2Y="><img src="img/icons/icons8-instagram-96.png" alt="Instagram"></a>
          <!-- <a href="mailto:test@test.com" class="btn btn-main">[@contact]</a> -->
        </div>
      </div> 
    </main>