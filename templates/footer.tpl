    <footer>
      <div class="container-fluid mt-3 bg-bars">
        <div class="row justify-content-around text-center">
          <div class="col-12 col-md-4 py-3">
            <a href="/legal.php">[@notice]</a>
          </div>
        </div>
        <div class="row pb-3">
          <div class="col-4 text-start">
            <span class="footer-stuff">Powered by <a href="https://wildions.com">Wildions</a></span>
          </div>
          <div class="col-4 text-center">
            <span class="footer-stuff">&copy; Alexandra Viret [@year]</span>
          </div>
          <div class="col-4 text-end">
            <span class="footer-stuff">[@icons] <a target="_blank" href="https://icons8.com">Icons8</a></span>
          </div>
        </div>
      </div>
    </footer>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-A3rJD856KowSb7dwlZdYEkO39Gagi7vIsF0jrRAoQmDKKtQBHUuLZ9AsSv4jD4Xa" crossorigin="anonymous"></script>
    <script src="js/blueimp-gallery.min.js"></script>
    <script>
      document.getElementById('links').onclick = function (event) {
        event = event || window.event
        var target = event.target || event.srcElement
        var link = target.src ? target.parentNode : target
        var options = { index: link, event: event }
        var links = this.getElementsByTagName('a')
        blueimp.Gallery(links, options)
      }
      blueimp.Gallery(document.getElementById('links-order').getElementsByTagName('a'), {
        container: '#blueimp-gallery-order',
        carousel: true
      })
      function collapseMenu() {
      	$("#nav-menu").collapse('hide');
      }
    </script>
    [@extra-scripts]
  </body>
</html>