<?php
$txt = [
  "Bienvenidos a mi sitio web",
  "Aviso legal y política de privacidad",
  "Iconos por",
  "Si quieres contactar conmigo para alguna de estas actividades",
  "Menú de navegación",
  "Mi nombre es Sachenka Vieternelle, soy nacida en Francia. Una de mis aficiones es dibujar almas, me gustan mucho las personas, sus mascotas y la energía que desprenden.</p><p>Siempre he sido muy sensible, algo que me ha acompañado toda la vida. He aprendido a comprender y expresar mi sensibilidad dibujando.</p><p>Mi camino interior me hizo desarrollar los dibujos de alma como herramienta para ayudar a los demás a recordarse quienes son, y ver su belleza De esta forma creo un dibujo en lámina de diferentes tamaños, pintado con lápices de colores, a partir de una foto o una entrevista personal.",
  "Habitualmente asisto a ferias para vender mis dibujos de almas y mis colecciones de cartas con mensajes hechos por mi. Soy una apasionada de las flores y me encanta fotografiarlas. Además ayudo a personas a poner orden en sus casas y entornos, con unas pautas siguiendo “Método de Marie Kondo”.",
  '"' . "La primera vez que vi los dibujos de las almas de Sachenka sentí en mi la belleza más pura de la Divinidad.",
  "En cada una de sus creaciones, Sachenka trae con amor las autenticas emanaciones de tu alma, el mandala de tu yo superior, todo lo que albergas en lo mas profundo de tu corazón, y que cuando lo contemplas es como si ya estuvieras tocando tu divinidad interior.",
  "Cada " . '"Dibujo del Alma"' . " es único e irrepetible, tal y como las almas son. Todas diferentes en su forma original de expresar la creación, en su forma de amar con la belleza de su música, su danza, de su aroma y su color.",
  "Un dibujo protector para ti, para tu hogar y para los demás.",
  "Gracias Sachenka por traer a la vida la más pura esencia de la Divinidad de las Almas." . '"',
  "MariLoli",
  "Además, si necesitas poner orden en tu vida, empieza por tu casa. Puedo asesorarte para organizar las estancias de tu casa, buscando así el equilibrio y el orden en tu entorno. Trabajo en Huesca y alrededores, recicla y/o desecha lo que ya no necesitas. El precio de tres horas es desde <strong>40 €uros</strong>, puedes consultar sin compromiso.",
  "<a href='https://es.wikipedia.org/wiki/Marie_Kondo' target='_blank 'rel='noreferrer'>LA MAGIA DEL ORDEN DE MARIE KONDO</a>",
  "Habitualmente asisto a ferias para vender mis dibujos de almas y mis colecciones de cartas con mensajes hechas por mi, que tienen un precio de 12 €uros.",
  "Testimonio",
  "<p>Es como comprender tu naturaleza, tu mundo interior, tu esencia.</p>
  <p>Aunque no se puede explicar con palabras, reconoces el gran potencial de armonía que te habita.</p>
  <p>Sientes que tu Ser es simplemente Amor, Paz, Creatividad... siento una gran calma al contemplar toda esa belleza que el miedo no me dejaba ver.</p>
  <p>Se ve reflejada la alegría y la inocencia que me llevan a vislumbrar la plenitud latente ahora y aquí.</p>
  <p>Una mirada directa a la Luz que somos.</p>
  <h3>Aintzane</h3>",
  "<p>Ponerme unos minutos frente al dibujo es encontrar un remanso de paz, de tranquilidad. Es un momento para escabullirme del desenfreno del día y permitirme mirar hacia mi interior.</p> 
  <p>En el dibujo veo los siete chakras de manera clara. Pongo consciencia en ellos y hace que por unos instantes pueda ponerlos en orden para continuar el día.</p> 
  <p>La interpretación de Sachenka una vez acabado el dibujo de alma también me parecieron de lo más acertadas. Es verdad que me considero alguien capaz de ayudar desinteresadamente a quien más lo necesita y que sabe escuchar para transformar las emociones.</p> 
  <p>Gracias Sachenka por tu delicada labor.</p>  
  <h3>María Antonia Sánchez Murillo</h3>",
  "<p>Asombro, Sachenka llegó a conectarse con la energía de mi gato, Minouche, un gato filosofó con la mirada penetrante.</p>
  <p>Ha captado intuitivamente la vibración que emana de éste. El irradia y transmite a todo su entorno su calorosa energía, que te calma.</p>
  <p>Después Sachenka examinó mi alma. ¿Qué ocurre dentro de mi, en mi ser profundo?</p>
  <p>He descubierto en su dibujo, sorprendida, que ha sentido de modo justo que estoy aquí sin estar. Que vivo en los aires pero anclada a mi relación con los demás.</p>
  <p>Su dibujo me hace pensar sobre mí, me abre puertas para crear una mejor armonía, una mejor vibración y me reconforta con lo que sé de mi.</p>
  <p>También me ha ayudado a comprender porque me es tan difícil de anclarme en esta tierra.</p>
  <h3>Sacha</h3>",
  "Mis dibujos de almas los suelo presentar en láminas de tres tamaños pero si deseas otro diferente, ¡no dudes en preguntarme!",
  "Testimonios",
  "Dibujos de Almas",
  "La magia del Orden",
  "Juego: Flores y palabras de sabiduría",
  "<p>El Juego consta de 42 cartas con fotos de flores con palabras de sabiduría.</p>
  <p>7 familias y 7 colores.</p> 
  <p>El precio del juego completo son 12 €uros. *</p>
  <p>* Sólo disponible en español.</p>",
  "<h2 class='text-center'>Política de privacidad y aviso legal</h2>
            <h3>Política de privacidad</h3>
            <p>A través de este sitio web no se recaban datos de carácter personal de los usuarios sin su conocimiento, ni se ceden a terceros.</p>
            <p>Con la finalidad de ofrecerle el mejor servicio y con el objeto de facilitar el uso, se analizan el número de páginas visitadas, el número de visitas, así como la actividad de los visitantes y su frecuencia de utilización. A estos efectos, la web de Sachenka Vieternelle utiliza la información estadística elaborada por el Proveedor de Servicios de Internet.</p>
            <p>La web de Sachenka Vieternelle no utiliza cookies para recoger información de los usuarios, ni registra las direcciones IP de acceso.</p>
            <p>La web de Sachenka Vieternelle contiene enlaces a sitios web de terceros, cuyas políticas de privacidad son ajenas a la de la AEPD. Al acceder a tales sitios web usted puede decidir si acepta sus políticas de privacidad y de cookies. Con carácter general, si navega por internet usted puede aceptar o rechazar las cookies de terceros desde las opciones de configuración de su navegador.</p>
            <h3>Aviso legal</h3>
            <p>En cumplimiento del artículo 10 de la Ley 34/2002, de 11 de julio, de Servicios de la Sociedad de la Información y Comercio Electrónico (LSSICE) a continuación se detallan los datos identificativos de la empresa:</p>
            <p>Nombre de empresa: Sachenka Viret (persona física)</p>
            <p>NIF: 80107100248</p>
            <p>Dirección: Ayerbe (Huesca)</p>
            <p>Teléfono: +33 637085 233</p>
            <p>Email: info@sachenkavieternelle.com</p>
            <h4>FINALIDAD DE LA PÁGINA WEB</h4>
            <p>La finalidad de esta web es únicamente informativa. El presente aviso legal regula el uso del sitio web: www.sachenkavieternelle.com</p>
            <h4>LEGISLACIÓN</h4>
            <p>Con carácter general las relaciones entre Sachenka Viret con los Usuarios de su página web se encuentran sometidas a la legislación y jurisdicción españolas.</p>
            <h4>USO Y ACCESO DE USUARIOS</h4>
            <p>El Usuario queda informado, y acepta, que el acceso a la presente web no supone, en modo alguno, el inicio de una relación comercial con Sachenka Viret.</p>
            <h4>PROPIEDAD INTELECTUAL E INDUSTRIAL</h4>
            <p>Los derechos de propiedad intelectual del contenido de las páginas web, su diseño gráfico y códigos son titularidad de Sachenka Viret y, por tanto, queda prohibida su reproducción, distribución, comunicación pública, transformación o cualquier otra actividad que se pueda realizar con los contenidos de su página web ni aun citando las fuentes, salvo consentimiento por escrito de Sachenka Viret.</p>
            <h4>CONTENIDO DE LA WEB Y ENLACES (LINKS)</h4>
            <p>Sachenka Viret no asume responsabilidad alguna por la información contenida en páginas web de terceros a las que se pueda acceder por 'links' o enlaces de la web www.sachenkavieternelle.com. La presencia de 'links' o enlaces en la página web de Sachenka Viret tiene finalidad meramente informativa y en ningún caso supone sugerencia, invitación o recomendación sobre los mismos.</p>"
]
?>